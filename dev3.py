from tgo import *
import numpy
n = 40

import matplotlib.pyplot as plot
#from matplotlib import pyplot as plot
xr = numpy.linspace(0, 15, 1000)
def f(x):
    return (x - 30) * numpy.sin(x)

def f2(x):
    return (x[0] - 30) * numpy.sin(x[1])


plot.figure(1)
plot.plot(xr, f(xr), 'k')

# Init tgo class
# Note: Using ints for irrelevant class inits like func
bounds = [(0, 15)]
bounds2 = [(0, 15), (0, 15), (0, 15)]
TGOc = TGO(f, bounds)
TGOc2 = TGO(f2, bounds2)

# generate sampling points
#TGOc.C = numpy.linspace(0, 15, 16)

#TGOc.n = 15
TGOc.n = n
TGOc2.n = n
TGOc.sampling()
TGOc2.sampling()

plot.plot(TGOc.C, f(TGOc.C), 'rx')


# initiate global storage containers for all minima
TGOc.x_min = []
TGOc.fun_min = []
T, H, F = TGOc.topograph()
T, H, F = TGOc2.topograph()
TGOc.stop = False
i = 0


from scipy.spatial import kdtree
#help(kdtree)

kdata = (1, TGOc.C)
kdata = TGOc.C
ATree = kdtree.KDTree(kdata)

if False:
    print('TGOc.C[0] = {}'.format(TGOc.C[0]))
    #print('TGOc.C = {}'.format(TGOc.C))
    print('ATree.query(TGOc.C[0], k=3) = {}'.format(ATree.query(TGOc.C[0], k=3)))
    print('TGOc.C[0] = {}'.format(TGOc.C[0]))
    print('TGOc.C[31] = {}'.format(TGOc.C[31]))
    print('TGOc.C[32] = {}'.format(TGOc.C[32]))

    print('ATree.query(TGOc.C[31], k=3) = {}'.format(ATree.query(TGOc.C[31], k=3)))
    #print('TGOc.C[0] = {}'.format(TGOc.C[0]))
    print('TGOc.C[31] = {}'.format(TGOc.C[31]))
    print('TGOc.C[32] = {}'.format(TGOc.C[32]))
    print('TGOc.C[15] = {}'.format(TGOc.C[15]))

print("NOTE: The new minima do not need to be appended to the tree:")
print('ATree.query([8.0], k=2) = {}'.format(ATree.query([8.0], k=2)))
print('TGOc.C[30] = {}'.format(TGOc.C[30]))
print('TGOc.C[33] = {}'.format(TGOc.C[33]))
#print('TGOc.C[14] = {}'.format(TGOc.C[14]))

#
print("="*60)
print("Split tree into two branches for two nearest point 30 and 33")
print('ATree.query(TGOc.C[30], k=3) = {}'.format(ATree.query(TGOc.C[30], k=3)))
print('TGOc.C[30] = {}'.format(TGOc.C[30]))
print('TGOc.C[33] = {}'.format(TGOc.C[33]))
print('TGOc.C[14] = {}'.format(TGOc.C[14]))
print("="*30)
print('ATree.query(TGOc.C[33], k=3) = {}'.format(ATree.query(TGOc.C[33], k=3)))
print('TGOc.C[33] = {}'.format(TGOc.C[33]))
print('TGOc.C[14] = {}'.format(TGOc.C[14]))
print('TGOc.C[30] = {}'.format(TGOc.C[30]))
print("="*60)
print("Check nearest points that are not 30 and 33:")
print('ATree.query(TGOc.C[14], k=4) = {}'.format(ATree.query(TGOc.C[14], k=4)))
print("="*60)
print('ATree.query(TGOc.C[17], k=10) = {}'.format(ATree.query(TGOc.C[17], k=10)))


print('ATree.query([8.0], '
      'k=len(TGOc.C)) = {}'.format(ATree.query([8.0], k=len(TGOc.C))))

#print('ATree.data = {}'.format(ATree.data))

if False:
    kdata = TGOc2.C
    print('kdata = {}'.format(kdata))
    ATree2 = kdtree.KDTree(kdata)
    print('TGOc2.C[0] = {}'.format(TGOc2.C[0]))
    #print('TGOc2.C = {}'.format(TGOc2.C))
    print('ATree.query(TGOc2.C[0], k=2) = {}'.format(ATree2.query(TGOc2.C[0], k=3)))
    print('TGOc2.C[0] = {}'.format(TGOc2.C[0]))
    print('TGOc2.C[24] = {}'.format(TGOc2.C[28]))
    print('TGOc2.C[32] = {}'.format(TGOc2.C[36]))


#plot.show()