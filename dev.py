from tgo import *
import numpy
# Init tgo class
# Note: Using ints for irrelevant class inits like func
TGOc = TGO(1, (0, 1))
# int bool solution for known sampling points
T_Ans = numpy.array([[0, 0, 0, 0, 0],
                     [0, 1, 1, 1, 1],
                     [1, 0, 0, 0, 0],
                     [1, 1, 1, 1, 1],
                     [0, 0, 0, 0, 1],
                     [1, 1, 0, 1, 0]])

T_Ans = T_Ans.astype(bool)

# Known order of sampling points
A = numpy.array([[2, 1, 5, 3, 4],
                 [3, 2, 5, 0, 4],
                 [0, 5, 1, 3, 4],
                 [1, 5, 2, 0, 4],
                 [5, 1, 2, 3, 0],
                 [2, 4, 1, 0, 3]])

# function values at test points
F = numpy.array([29, 5, 25.81, 1, 25, 20])

# Sampling points used in Henderson example
TGOc.C = numpy.array([[2, 5],  # P1
                      [1, 2],  # P2
                      [3, 4],  # P3
                      [0, 1],  # P4
                      [5, 0],  # P5
                      [4, 2]  # P6
                      ])

# func used
def f_sub(x):
    return x[0]**2 + x[1]**2

TGOc.func = f_sub

T, H, F = TGOc.topograph()

print("T = {}".format(T))
print("H = {}".format(H))
print("F = {}".format(F))

a = numpy.array([[0, 0]])
fun_a = 0.0

#T, H, F = TGOc.atopograph(a, fun_a)
T, H, F = TGOc.augtopograph(a, fun_a)

print("T = {}".format(T))
print("H = {}".format(H))
print("F = {}".format(F))

# a = numpy.array([[2, 2]])
# fun_a = -10
# T, H, F = TGOc.augtopograph(a, fun_a)
# print("T = {}".format(T))
# print("H = {}".format(H))
# print("F = {}".format(F))


# a = numpy.array([[10, 10]])
# fun_a = -10
# T, H, F = TGOc.augtopograph(a, fun_a)
# print("T = {}".format(T))
# print("H = {}".format(H))
# print("F = {}".format(F))
