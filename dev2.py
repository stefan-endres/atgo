from tgo import *
import numpy
n = 40

import matplotlib.pyplot as plot
#from matplotlib import pyplot as plot
xr = numpy.linspace(0, 15, 1000)
def f(x):
    return (x - 30) * numpy.sin(x)

plot.figure(1)
plot.plot(xr, f(xr), 'k')




# Init tgo class
# Note: Using ints for irrelevant class inits like func
bounds = [(0, 15)]
TGOc = TGO(f, bounds)

# generate sampling points
#TGOc.C = numpy.linspace(0, 15, 16)

#TGOc.n = 15
TGOc.n = n
TGOc.sampling()

plot.plot(TGOc.C, f(TGOc.C), 'rx')
plot.show()

# initiate global storage containers for all minima
TGOc.x_min = []
TGOc.fun_min = []
T, H, F = TGOc.topograph()
TGOc.stop = False
i = 0
# while not TGOc.stop:
#     if True:  # Dev prints
#         print("T = {}".format(T))
#         print("C = {}".format(TGOc.C.T))
#         print("H = {}".format(H))
#         print("F = {}".format(F))
#         i += 1
#         print('iteration = {}'.format(i))
#
#     min_ind = TGOc.minimizers(T)
#     # Find a local minima from global minimiser
#     lres = TGOc.minima(min_ind)
#     # Find subspace containing basin
#     Ha, Fa = TGOc.atopograph(TGOc.lx, TGOc.lfun)
#     # Cut basin subspace from current feasible set
#     if not TGOc.stop:
#         T, H, F = TGOc.basincut(TGOc.indcut)
#     else:
#         print('STOP')

if False:
    from tgo import atgo
    #res = atgo(f, bounds, n=13)
    res = atgo(f, bounds, n=n)
    print(res)


    def f2(x):
        return x[0] ** 2 + x[1] ** 2

    bounds2=[(-1, 6), (-1, 6)]
    res2 = atgo(f2, bounds2, n=n)
    print(res2)

    bounds2=[(0, 1), (0, 1)]
    res2 = atgo(f2, bounds2, n=n)
    print(res2)


    bounds = [(0, 60)]


    def g(x):
        return 58 - numpy.sum(x, axis=0)

    res = tgo(f, bounds, n=n)
    print(res)
    ares = atgo(f, bounds, n=n, g_cons=g)
    print(ares)

#plot.show()
# [stefan_endres @ primary atgo]$ python2 tgo.py
# fun: 1.2355844251934306e-12
# funl: array([1.23558443e-12, 2.00648149e-11, 2.02434607e-11,
#              2.27403008e-11])
# message: 'Optimization terminated successfully.'
# nfev: 503
# nlfev: 403
# nljev: 0
# succes: True
# x: array([1.00000017, 1.00000023])
# xl: array([[1.00000017, 1.00000023],
#            [0.99999552, 0.99999104],
#            [0.9999955, 0.99999101],
#            [0.99999525, 0.99999046]])



#     Rosenbrock
#     function... == == == == == == == == == == == == == == == == == == == ==
#     == == == == == == == == == == == == == == == == == == == == == == == ==
#     == == == == == ==
#     == == == == == == == == == == == == == == == == == == == == == == == ==
#     == == == == == == == == == == == == == == == == == == == == == == == ==
#     == ==
#     Topographical
#     Global
#     Optimization:
#     ----------------------------------
#     fun: 1.1413322514791322e-11
# funl: array([1.14133225e-11, 1.49480911e-11, 1.98798440e-11,
#              2.01736336e-11, 2.03840555e-11])
# message: 'Optimization terminated successfully.'
# nfev: 741
# nlfev: 641
# nljev: 0
# succes: True
# x: array([0.99999688, 0.99999363])
# xl: array([[0.99999688, 0.99999363],
#            [0.99999669, 0.99999318],
#            [0.99999554, 0.99999108],
#            [0.99999551, 0.99999101],
#            [0.99999549, 0.99999096]])



#     Scalar
#     opt
#     test
#     on
#     f(x) = (x - 30) * sin(
#         x)... == == == == == == == == == == == == == == == == == == == == ==
#     == == == == == == == == == == == == == == == == == == == == == == == ==
#     == == == == ==
#     == == == == == == == == == == == == == == == == == == == == == == == ==
#     == == == == == == == == == == == == == == == == == == == == == == == ==
#     == ==
#     Topographical
#     Global
#     Optimization:
#     ----------------------------------
#     fun: -28.446771324181217
# funl: array([-28.44677132, -24.99785984, -22.16855376, -18.72136195,
#              -15.89423937, -12.45154942, -9.63133158, -6.20801301,
#              -3.43727232, -0.46353338])
# message: 'Optimization terminated successfully.'
# nfev: 246
# nlfev: 146
# nljev: 0
# succes: True
# x: array([1.53567906])
# xl: array([[1.53567906],
#            [55.01782167],
#            [7.80894889],
#            [48.74797467],
#            [14.07445705],
#            [42.49138562],
#            [20.31743866],
#            [36.28607531],
#            [26.43039613],
#            [30.76370931]])



# Eggholder function ... ====================================================================================================
# ====================================================================================================
# Topographical Global Optimization:
# ----------------------------------
#      fun: -959.64066272085051
#     funl: array([-959.64066272, -786.52599408, -718.16745962, -582.30628005,
#        -565.99778097, -559.78685655, -557.85777903, -493.9605115 ,
#        -426.48799655, -419.31194957])
#  message: 'Optimization terminated successfully.'
#     nfev: 591
#    nlfev: 491
#    nljev: 0
#   succes: True
#        x: array([ 512.        ,  404.23180542])
#       xl: array([[ 512.        ,  404.23180542],
#        [-456.88574619, -382.6233161 ],
#        [ 283.07593402, -487.12566542],
#        [ 324.99187533,  216.0475439 ],
#        [-105.87688985,  423.15324143],
#        [-242.97923629,  274.38032063],
#        [-414.8157022 ,   98.73012628],
#        [ 150.2320956 ,  301.31377513],
#        [  91.00922754, -391.28375925],
#        [ 361.66626134, -106.96489228]])

